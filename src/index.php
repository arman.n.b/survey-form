<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sorvey Form</title>
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>

<body>
    <div class="conter">
        <div class="right-content">
            <form action="procces.php" method="POST">
                <input type="text" placeholder="Your Name . . . " class="input-name" name="name" autocomplete="off">
                <div class="radio">
                    <label for="team1">
                        <input type="radio" name="team" id="team1" class="input-radio radio-left" style="margin-right: 22px;" value="barcelona">
                        FC Barcelona
                    </label>
                    <br>
                    <label for="team2">
                        <input type="radio" name="team" id="team2" class="input-radio" value="atmadrid">
                        Atletico Madrid
                    </label>
                    <br>
                    <label for="team3">
                        <input type="radio" name="team" id="team3" class="input-radio radio-left" style="margin-right: 30px;" value="realmadrid">
                        Real Madrid
                    </label>
                </div>
                <input type="submit" value="Voting" class="btn" name="btn">
            </form>
        </div>
        <div class="left-content">
            <p class="title">Voting Result</p>
            <div class="voting">
                <p class="result">Real Madrid : <span>
                        <?php
                        if (isset($_SESSION["realMadrid"]) && !empty($_SESSION["realMadrid"])) {
                            echo $_SESSION["realMadrid"] - 1;
                        } else {
                            echo "0";
                        }
                        ?> Vote</span></p>
                <p class="result">Atletico Madrid : <span>
                        <?php
                        if (isset($_SESSION["atleticoMadrid"]) && !empty($_SESSION["atleticoMadrid"])) {
                            echo $_SESSION["atleticoMadrid"] - 1;
                        } else {
                            echo "0";
                        }
                        ?>
                        Vote</span></p>
                <p class="result">FC Barcelona : <span>
                        <?php
                        if (isset($_SESSION["barcelona"]) && !empty($_SESSION["barcelona"])) {
                            echo $_SESSION["barcelona"] - 1;
                        } else {
                            echo "0";
                        }
                        ?>
                        Vote</span></p>
            </div>
            <?php if (isset($_SESSION["msg"]) && !empty($_SESSION["msg"])) { ?>
                <div class="alert">
                    <?php
                    echo $_SESSION["msg"];
                    unset($_SESSION["msg"]);
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>