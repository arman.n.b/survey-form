<?php
session_start();
$msg = null;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['btn']) && $_POST["btn"] == "Voting") {
        if (!empty($_POST["name"]) && !empty($_POST["team"])) {
            $name = $_POST["name"];
            $content = file_get_contents("users.txt");
            $expload = explode("=>", $content);
            if (in_array($name, $expload)) {
                $msg = "You have voted once";
            } else {
                $stream1 = fopen("users.txt", "a+");
                fwrite($stream1, "$name=>");
                fclose($stream1);
                $team = $_POST["team"];
                if ($team == "realmadrid") {
                    $dir = "real-madrid.txt";
                } elseif ($team == "atmadrid") {
                    $dir = "atletico-madrid.txt";
                } elseif ($team == "barcelona") {
                    $dir = "barcelona.txt";
                }
                $stream = fopen($dir, "a+");
                fwrite($stream , "1");
                fclose($stream);
            }
        } else {
            $msg = "Fill in The blanks";
        }
    }
    // real_madrid  counting the votes
    $rstream = fopen("real-madrid.txt" , "r");
    $rcounter = 0;
    while( !feof($rstream) ){
        $char = fgetc($rstream);
        $rcounter++;
    }
    fclose($rstream);
    
    // atletico_madrid  counting the votes
    $astream = fopen("atletico-madrid.txt" , "r");
    $acounter = 0;
    while( !feof($astream) ){
        $char = fgetc($astream);
        $acounter++;
    }
    fclose($astream);
    
    // barcelona  counting the votes
    $bstream = fopen("barcelona.txt" , "r");
    $bcounter = 0;
    while(!feof($bstream)){
        $char = fgetc($bstream);
        $bcounter++;
    }
    fclose($bstream);
}
$_SESSION["msg"] = $msg;
$_SESSION["realMadrid"] = $rcounter;
$_SESSION["atleticoMadrid"] = $acounter;
$_SESSION["barcelona"] = $bcounter;
header("location:index.php");